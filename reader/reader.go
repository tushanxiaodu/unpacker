package reader

import (
	"encoding/binary"
	"math"
)

type Reader struct {
	s         []byte
	i         uint32
	bigEndian bool
}

func NewReader(bytes []byte, bigEndian bool) *Reader {
	return &Reader{bytes, 0, bigEndian}
}

func (r *Reader) Uint8() (value uint8) {
	value = uint8(r.s[r.i])
	r.i++
	return
}

func (r *Reader) Int8() (value int8) {
	value = int8(r.s[r.i])
	r.i++
	return
}

func (r *Reader) Uint16() (value uint16) {
	if r.bigEndian {
		value = binary.BigEndian.Uint16(r.s[r.i:])
	} else {
		value = binary.LittleEndian.Uint16(r.s[r.i:])
	}
	r.i += 2
	return
}

func (r *Reader) Int16() (value int16) {
	return int16(r.Uint16())
}

func (r *Reader) Uint32() (value uint32) {
	if r.bigEndian {
		value = binary.BigEndian.Uint32(r.s[r.i:])
	} else {
		value = binary.LittleEndian.Uint32(r.s[r.i:])
	}
	r.i += 4
	return
}
func (r *Reader) Int32() (value int32) {
	return int32(r.Uint32())
}

func (r *Reader) Uint64() (value uint64) {
	if r.bigEndian {
		value = binary.BigEndian.Uint64(r.s[r.i:])
	} else {
		value = binary.LittleEndian.Uint64(r.s[r.i:])
	}
	r.i += 8
	return
}
func (r *Reader) Int64() (value int64) {
	return int64(r.Uint64())
}

func (r *Reader) Float32() (value float32) {
	return math.Float32frombits(r.Uint32())
}

func (r *Reader) Float64() (value float64) {
	return math.Float64frombits(r.Uint64())
}

func (r *Reader) String(offset uint32) (value string) {
	for i := int(offset); i < len(r.s); i++ {
		if r.s[i] == 0 {
			return string(r.s[offset:i])
		}
	}
	return string(r.s[offset:])
}

func (r *Reader) Bytes(offset uint32, size uint32) (value []byte) {
	return r.s[offset : offset+size]
}

func (r *Reader) Seek(offset uint32) {
	r.i = offset
}

func (r *Reader) Tell() uint32 {
	return r.i
}
