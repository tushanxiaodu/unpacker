package utf

import "gitlab.com/kirafan/cri/unpacker/reader"

type UTFTable struct {
	*reader.Reader
	Header
	Name        string
	TableSize   uint32
	Constants   map[string]interface{}
	DynamicKeys []DynamicKey
	Rows        []map[string]interface{}
}

type Header struct {
	U1                uint16
	RowOffset         uint16
	StringTableOffset uint32
	DataOffset        uint32
	TableNameOffset   uint32
	NumberOfFields    uint16
	RowSize           uint16
	NumberOfRows      uint32
}

type DynamicKey struct {
	Name string
	Type uint8
}

func NewUTFTable(data []byte) *UTFTable {
	if string(data[:4]) != "@UTF" {
		return nil
	}
	t := &UTFTable{
		Reader:    reader.NewReader(data[8:], true),
		TableSize: uint32(len(data) - 8),
		Constants: make(map[string]interface{}),
	}
	t.Header = Header{
		U1:                t.Uint16(),
		RowOffset:         t.Uint16(),
		StringTableOffset: t.Uint32(),
		DataOffset:        t.Uint32(),
		TableNameOffset:   t.Uint32(),
		NumberOfFields:    t.Uint16(),
		RowSize:           t.Uint16(),
		NumberOfRows:      t.Uint32(),
	}
	t.Name = t.String(t.StringTableOffset + t.TableNameOffset)

	t.Seek(0x18)
	for i := uint16(0); i < t.NumberOfFields; i++ {
		fieldType := t.Uint8()
		nameOffset := t.Uint32()
		occurrence := fieldType & columnStorageMask
		typeKey := fieldType & columnTypeMask
		name := t.String(t.StringTableOffset + nameOffset)
		if occurrence == columnStorageConstant || occurrence == columnStorageConstant2 {
			t.Constants[name] = t.readType(typeKey)
		} else {
			t.DynamicKeys = append(t.DynamicKeys, DynamicKey{name, typeKey})
		}
	}

	t.Seek(uint32(t.RowOffset))
	for i := uint32(0); i < t.NumberOfRows; i++ {
		row := make(map[string]interface{})
		for _, dynamicKey := range t.DynamicKeys {
			row[dynamicKey.Name] = t.readType(dynamicKey.Type)
			if data, ok := row[dynamicKey.Name].([]byte); ok {
				if string(data[:4]) == "@UTF" {
					row[dynamicKey.Name] = NewUTFTable(data)
				}
			}
		}
		for key, value := range t.Constants {
			row[key] = value
		}
		t.Rows = append(t.Rows, row)
	}

	return t
}

func (t *UTFTable) readType(typeKey uint8) interface{} {
	switch typeKey {
	case 0:
		return t.Uint8()
	case 1:
		return t.Int8()
	case 2:
		return t.Uint16()
	case 3:
		return t.Int16()
	case 4:
		return t.Uint32()
	case 5:
		return t.Int32()
	case 6:
		return t.Uint64()
	case 7:
		return t.Int64()
	case 8:
		return t.Float32()
	case 9:
		return t.Float64()
	case 10:
		offset := t.Uint32()
		return t.String(t.StringTableOffset + offset)
	case 11:
		offset := t.Uint32()
		size := t.Uint32()
		return t.Bytes(t.DataOffset+offset, size)
	}
	return nil
}
