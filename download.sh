wget --no-check-certificate https://jaist.dl.sourceforge.net/project/lame/lame/3.100/lame-3.100.tar.gz
ls
tar -zxf lame-3.100.tar.gz
cd lame-3.100
./configure --silent
make --silent
make install --silent
cd ..

unpacker $1

for wav in $(ls cri/*/*.wav); do echo lame $wav; lame --silent $wav; done
rm cri/*/*.wav

ls cri
