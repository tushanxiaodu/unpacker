package afs

import (
	"fmt"

	"gitlab.com/kirafan/cri/unpacker/reader"
)

type AFSArchive struct {
	*reader.Reader
	NumberOfFiles uint32
	Alignment     uint32
	OffsetSize    uint8
	FileIDs       []uint16
	FileOffsets   []uint32
	Files         map[uint16][]byte
}

func NewAFSArchive(data []byte) *AFSArchive {
	if string(data[:4]) != "AFS2" {
		return nil
	}
	a := &AFSArchive{
		Reader: reader.NewReader(data, false),
	}
	_ = a.Uint32()
	_, a.OffsetSize, _, _ = a.Uint8(), a.Uint8(), a.Uint8(), a.Uint8()
	a.NumberOfFiles = a.Uint32()
	a.Alignment = a.Uint32()

	for i := uint32(0); i < a.NumberOfFiles; i++ {
		a.FileIDs = append(a.FileIDs, a.Uint16())
	}
	for i := uint32(0); i <= a.NumberOfFiles; i++ {
		var offset uint32
		if a.OffsetSize == 2 {
			offset = uint32(a.Uint16())
		} else {
			offset = a.Uint32()
		}
		if offset%a.Alignment != 0 {
			offset += a.Alignment - offset%a.Alignment
		}
		a.FileOffsets = append(a.FileOffsets, offset)
	}
	a.Files = make(map[uint16][]byte)
	for i := uint32(0); i < a.NumberOfFiles; i++ {
		a.Files[a.FileIDs[i]] = a.Bytes(a.FileOffsets[i], a.FileOffsets[i+1]-a.FileOffsets[i])
	}

	return a
}

// String implements Stringer
func (archive AFSArchive) String() string {
	return fmt.Sprintf("AFSArchive{NumberOfFiles:%v Alignment:%v OffsetSize:%v FileIDs:%v FileOffsets:%v}",
		archive.NumberOfFiles,
		archive.Alignment,
		archive.OffsetSize,
		archive.FileIDs,
		archive.FileOffsets,
	)
}
